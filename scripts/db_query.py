# -*- coding: UTF-8 -*-
import psycopg2, psycopg2.extras
from collections import OrderedDict
from datetime import datetime
from encrypt_psswd import encrypt
import simplejson as json
host_dev = 'localhost'
host_prod = 'gymassistant.cell2vonehcx.us-east-1.rds.amazonaws.com'
host = host_prod
dbname = 'ga_database'
dbuser = 'gauser'
dbpassword = 'gapassword'

#########################################
# INICIO DE SESIÓN EN LA PLATAFORMA
########################################
def login(user_email,password):
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        cur = conn.cursor()
    except:
        return 'ERROR: imposible conectar a la base de datos'
    
    query = """
    SELECT id,email,name,last_name,surname,type_of_user_id 
    FROM users WHERE email = '{}' AND password = '{}'
    """.format(user_email,password)

    try:
        cur.execute(query)
        user_info = OrderedDict()
        for row in cur:
            user_info['id'] = int(row[0])
            user_info['email'] = row[1]
            user_info['name'] = row[2]
            user_info['last_name'] = row[3]
            user_info['surname'] = row[4]
            user_info['type_of_user'] = int(row[5])
        cur.close()
        return user_info
    except Exception as e:
        return e


#########################################
# REGISTRAR UN MIEMBRO NUEVO
########################################   
def register_member(member_info):
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 

    try:
        query = u"""
        INSERT INTO users(email,password,name,last_name,surname,type_of_user_id)
        VALUES ('{mi[member_email]}','{mi[password]}','{mi[name]}','{mi[last_name]}','{mi[surname]}',1);

        INSERT INTO member_details(weight,height,waist,imc,member_id,routine_id,diet_id,instructor_id)
        VALUES ({mi[weight]},{mi[height]},{mi[waist]},{mi[imc]},
        (SELECT id FROM users WHERE email = '{mi[member_email]}'),
        {mi[routine]},
        {mi[diet]},
        {mi[instructor_id]});

        INSERT INTO member_history(weight,height,waist,imc,member_id,instructor_id,routine_id,diet_id)
        VALUES({mi[weight]},{mi[height]},{mi[waist]},{mi[imc]},
        (SELECT id FROM users WHERE email = '{mi[member_email]}'),
        {mi[instructor_id]},
        {mi[routine]},
        {mi[diet]});

        INSERT INTO coachings(instructor_id,member_id)
        VALUES ({mi[instructor_id]},
        (SELECT id FROM users WHERE email = '{mi[member_email]}'))
        """.format(mi = member_info)   
        cur.execute(query)
        cur.close()
        return 200
    except Exception as e:
        print('El error es: ',e)
        return 400

#########################################
# REGISTRAR EL AVANCE DE UN MIEMBRO
########################################
def progress_record(member_info):
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 
    
    try:
        query = u"""
        INSERT INTO member_history(member_id,instructor_id,weight,height,waist,imc,routine_id,diet_id)
        VALUES ({mi[member_id]},
        (SELECT instructor_id FROM coachings WHERE member_id = {mi[member_id]}),
        {mi[weight]},
        {mi[height]},
        {mi[waist]},
        {mi[imc]},
        (SELECT routine_id FROM member_details WHERE member_id = {mi[member_id]}),
        (SELECT routine_id FROM member_details WHERE member_id = {mi[member_id]}));

        UPDATE member_details 
        SET routine_id = {mi[new_routine]},
        diet_id = {mi[new_diet]}
        
        """.format(mi = member_info)  

        cur.execute(query)
        cur.close()
    except Exception as e:
        print(e)
        return e

#########################################
# OBTENER LA INFO DE UN MIEMBRO PARA MOSTRARLA
########################################
def get_member_info(member_info):

    response = OrderedDict()
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 

    try:
        query = u"""
        SELECT * FROM users WHERE id = {mi[member_id]}
        """.format(mi = member_info)  
        cur.execute(query)
        personal_info = OrderedDict()
        for row in cur:
            personal_info['member_id'] = row[0]
            personal_info['name'] = row[3]
            personal_info['last_name'] = row[4]
            personal_info['surname'] = row[5]
        
        response['personal_info'] = personal_info

        query = u"""
        SELECT * FROM member_details WHERE member_id = {mi[member_id]}
        """.format(mi = member_info)  
        cur.execute(query)
        detailed_info = OrderedDict()
        for row in cur:
            detailed_info['weight'] = row[1]
            detailed_info['height'] = row[2]
            detailed_info['waist'] = row[3]
            detailed_info['imc'] = row[4]
            detailed_info['routine_id'] = row[6]
            detailed_info['diet_id'] = row[7]
            detailed_info['instructor_id'] = row[8]
        
        response['detailed_info'] = detailed_info

        query = u"""
        SELECT * FROM member_history WHERE member_id = {mi[member_id]}
        """.format(mi = member_info)  
        cur.execute(query)
        progress_info = []
        for row in cur:
            progress = OrderedDict()
            progress['weight'] = row[3]
            progress['height'] = row[4]
            progress['waist'] = row[5]
            progress['imc'] = row[6]
            date = str(row[9]).split(' ')[0].split('-')
            day = date[2]
            month = date[1]
            year = date[0]
            progress['date'] = '{}/{}/{}'.format(day,month,year)
            # progress['routine_id'] = row[7]
            # progress['diet_id'] = row[8]

            progress_info.append(progress)

        
        response['progress_info'] = progress_info
        cur.close()
        return response

    except Exception as e:
        print(e)
        return e
    #print(json.dumps(response))


#####################################################
# OBTENER LA INFO DE LOS ASESORADOS DE UN INSTRUCTOR
#####################################################

def get_members_by_instructor(instructor_id):
    response = OrderedDict()
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 
    
    try:
        query = u"""
        SELECT id,email,name,last_name,surname,weight,waist,imc
        FROM users 
        INNER JOIN (SELECT member_id,instructor_id,weight,waist,imc FROM member_details WHERE instructor_id = {})
        AS member_details ON users.id = member_details.member_id
        """.format(instructor_id)  
        cur.execute(query)
        users = []
        for row in cur:
            user = OrderedDict()
            user['id'] = row[0]
            user['email'] = row[1]
            user['name'] = row[2]
            user['last_name'] = row[3]
            user['surname'] = row[4]
            user['weight'] = row[5]
            user['waist'] = row[6]
            user['imc'] = row[7]
            users.append(user)

        cur.close()
        return users

    except Exception as e:
        print(e)
        return e

#################################
# OBTENER LA LISTA DE RUTINAS
################################

def get_routines():
    response = OrderedDict()
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 
    
    try:
        query = u"""
        SELECT id,name
        FROM routines
        """
        cur.execute(query)
        routines = []
        for row in cur:
            routine = OrderedDict()
            routine['id'] = row[0]
            routine['name'] = row[1]

            routines.append(routine)

        cur.close()
        return routines

    except Exception as e:
        print(e)
        return e

#################################
# OBTENER LA LISTA DE DIETAS
################################

def get_diets():
    response = OrderedDict()
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 
    
    try:
        query = u"""
        SELECT id,name
        FROM diets
        """
        cur.execute(query)
        diets = []
        for row in cur:
            diet = OrderedDict()
            diet['id'] = row[0]
            diet['name'] = row[1]

            diets.append(diet)
        
        cur.close()
        return diets

    except Exception as e:
        print(e)
        return e


#################################
# OBTENER LA LISTA DE INSTRUCTORES
################################

def get_instructors():
    response = OrderedDict()
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 
    
    try:
        query = u"""
        SELECT id,name,last_name,surname
        FROM users 
        WHERE type_of_user_id = 2
        """
        cur.execute(query)
        instructors = []
        for row in cur:
            instructor = OrderedDict()
            instructor['id'] = row[0]
            instructor['name'] = row[1]
            instructor['last_name'] = row[2]
            instructor['surname'] = row[3]
            instructors.append(instructor)
        
        cur.close()
        return instructors

    except Exception as e:
        print(e)
        return e



#################################
# OBTENER DETALLE DE DIETA
################################

def get_diet_detail(diet_id):
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 
    
    try:
        query = u"""
        SELECT *
        FROM diets
        WHERE id = {}
        """.format(diet_id)
        cur.execute(query)
        diet = OrderedDict()
        for row in cur:
            mon = OrderedDict()
            tue = OrderedDict()
            wed = OrderedDict()
            thu = OrderedDict()
            fri = OrderedDict()
            sat = OrderedDict()
            sun = OrderedDict()
            days = []

            diet['id'] = row[0]
            diet['name'] = row[1]
            

            mon['day'] = 'Lunes'
            mon['instructions'] = row[2]
            days.append(mon)

            tue['day'] = 'Martes'
            tue['instructions'] = row[3]
            days.append(tue)

            wed['day'] = 'Miércoles'
            wed['instructions'] = row[4]
            days.append(wed)

            thu['day'] = 'Jueves'
            thu['instructions'] = row[5]
            days.append(thu)

            fri['day'] = 'Viernes'
            fri['instructions'] = row[6]
            days.append(fri)

            sat['day'] = 'Sábado'
            sat['instructions'] = row[7]
            days.append(sat)

            sun['day'] = 'Domingo'
            sun['instructions'] = row[8]
            days.append(sun)


            diet['days'] = days
        
        cur.close()
        return diet

    except Exception as e:
        print(e)
        return e    

#################################
# OBTENER DETALLE DE RUTINA
################################

def get_routine_detail(routine_id):
    try:
        conn = psycopg2.connect(dbname=dbname, host=host, user=dbuser, password=dbpassword)
        conn.autocommit = True
        cur = conn.cursor()
    except Exception as e:
        print('ERROR: imposible conectar a la base de datos ',e)
        return e 
    
    try:
        query = u"""
        SELECT *
        FROM routines
        WHERE id = {}
        """.format(routine_id)
        cur.execute(query)
        routine = OrderedDict()
        for row in cur:
            mon = OrderedDict()
            tue = OrderedDict()
            wed = OrderedDict()
            thu = OrderedDict()
            fri = OrderedDict()
            sat = OrderedDict()
            sun = OrderedDict()
            days = []

            routine['id'] = row[0]
            routine['name'] = row[1]
            

            mon['day'] = 'Lunes'
            mon['instructions'] = row[2]
            days.append(mon)

            tue['day'] = 'Martes'
            tue['instructions'] = row[3]
            days.append(tue)

            wed['day'] = 'Miércoles'
            wed['instructions'] = row[4]
            days.append(wed)

            thu['day'] = 'Jueves'
            thu['instructions'] = row[5]
            days.append(thu)

            fri['day'] = 'Viernes'
            fri['instructions'] = row[6]
            days.append(fri)

            sat['day'] = 'Sábado'
            sat['instructions'] = row[7]
            days.append(sat)

            sun['day'] = 'Domingo'
            sun['instructions'] = row[8]
            days.append(sun)


            routine['days'] = days
        
        cur.close()
        return routine

    except Exception as e:
        print(e)
        return e    


# member_info = OrderedDict()
# member_info['member_email'] = 'prueba7@gmail.com'
# member_info['member_id'] = 7
# member_info['password'] = encrypt('12345')
# member_info['display_name'] = 'Fulanito'
# member_info['name'] = 'Fulanito'
# member_info['last_name'] = 'Pérez'
# member_info['surname'] = 'Prado'
# member_info['weight'] = 62.758
# member_info['height'] = 63.758
# member_info['waist'] = 65.758
# member_info['routine'] = 1
# member_info['diet'] = 1
# member_info['instructor_id'] = 3
# member_info['imc'] = 28.0
# member_info['new_routine'] = 2
# member_info['new_diet'] = 2

#print(register_member(member_info))
#login('instructor@prueba.com','12345')
#progress_record(member_info)

#print(json.dumps(get_member_info(member_info),indent=4))
#print(json.dumps(get_members_by_instructor(3),indent=4))
#print(json.dumps(get_routines(),indent=4))
#print(json.dumps(get_diets(),indent=4))
#print(json.dumps(get_instructors(),indent=4))
#print(get_routine_detail(1))