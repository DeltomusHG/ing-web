import hashlib

def encrypt(thing_to_encrypt):
    return hashlib.sha1(thing_to_encrypt.encode('utf-8')).hexdigest()


#print(encrypt(input('Cadena a encriptar: ')))