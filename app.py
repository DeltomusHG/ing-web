from flask import Flask
from flask import render_template,request,redirect,url_for
import sys,os,requests
import simplejson as json
sys.path.insert(0,'scripts')
from db_query import login,register_member,progress_record
from db_query import get_member_info,get_members_by_instructor
from db_query import get_routines,get_diets,get_instructors, get_diet_detail, get_routine_detail
from collections import OrderedDict
app = Flask(__name__)
member_id = 0
instructor_id = 0
detailed_info = OrderedDict()

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login_instructors',methods = ['GET','POST'])
def login_instructors():
    global instructor_id
    if request.method == 'POST':
        user_info = login(request.form['user'],request.form['psswd'])
        print(user_info)
        if not user_info:
            print('empty answer')
            return redirect(url_for('index'))
        else:
            print('login correct')
            instructor_id = user_info['id']
            return redirect(url_for('instructor_dashboard'))
    return render_template('login_instructors.html')

@app.route('/login_members',methods = ['GET','POST'])
def login_members():
    global member_id
    if request.method == 'POST':
        user_info = login(request.form['user'],request.form['psswd'])
        print(user_info)
        if not user_info:
            print('empty answer')
            return redirect(url_for('index'))
        else:
            print('login correct')
            member_id = user_info['id']
            return redirect(url_for('member_dashboard'))

    return render_template('login_members.html')

@app.route('/register', methods = ['GET','POST'])
def register():
    routines = []
    diets = []
    instructors = []
    routines = get_routines()
    diets = get_diets()
    instructors = get_instructors()
    register_info = OrderedDict()
    if request.method == 'POST':
        print('Entro una request')
        register_info['member_email'] = request.form['email']
        register_info['password'] = request.form['password']
        register_info['name'] = request.form['name']
        register_info['last_name'] = request.form['last_name']
        register_info['surname'] = request.form['surname']
        register_info['weight'] = request.form['weight']
        register_info['height'] = request.form['height']
        register_info['waist'] = request.form['waist']
        register_info['imc'] = request.form['imc']
        register_info['instructor_id'] = request.form['instructor-selector']
        register_info['routine'] = request.form['routine-selector']
        register_info['diet'] = request.form['diet-selector']

        response = register_member(register_info)
        if response == 200:
            return redirect(url_for('login_members'))
        elif response == 400:
            return redirect(url_for('index'))

    return render_template('register.html',routines_list = routines, diets_list = diets, instructors_list = instructors)

@app.route('/member_dashboard', methods = ['GET','POST'])
def member_dashboard():
    global detailed_info
    member_info = OrderedDict()
    member_info['member_id'] = member_id
    full_info = get_member_info(member_info)
    print('Información detallada:')
    print(json.dumps(full_info,indent = 4))
    diet_info = get_diet_detail(full_info['detailed_info']['diet_id'])
    print(json.dumps(diet_info,indent=4))
    routine_info = get_routine_detail(full_info['detailed_info']['routine_id'])
    print(json.dumps(routine_info,indent=4))
    return render_template('member_dashboard.html',personal_info = full_info['personal_info'],detailed_info = full_info['detailed_info'],progress_info = full_info['progress_info'],diet_info = diet_info,routine_info = routine_info)

@app.route('/instructor_dashboard', methods = ['GET','POST'])
def instructor_dashboard():
    members = []
    members = get_members_by_instructor(instructor_id)
    return render_template('instructor_dashboard.html',members = members)

@app.route('/progress_register',methods = ['GET','POST'])
def progress_register():
    member_info = OrderedDict()
    routines = get_routines()
    diets = get_diets()
    if request.method == 'POST':
        member_id = request.args.get('id')

        member_info['weight'] = request.form['weight']
        member_info['height'] = request.form['height']
        member_info['waist'] = request.form['waist']
        member_info['imc'] = request.form['imc']
        member_info['new_routine'] = request.form['routine-selector']
        member_info['new_diet'] = request.form['diet-selector']
        member_info['member_id'] = member_id
        progress_record(member_info)

    return render_template('progress_register.html',routines_list = routines,diets_list = diets)


if __name__ == '__main__':
    app.run(debug = True)